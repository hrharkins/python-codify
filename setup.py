import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="codify",
    version="0.0.1",
    author="Rich Harkins",
    author_email="rich.harkins@gmail.com",
    description="Build Python via structures",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hrharkins/python-datamsg",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)

