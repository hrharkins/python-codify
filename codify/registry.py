
##############################################################################
##############################################################################

class GlobalRegistry(dict):
    '''
    Manages a set of globals for codify, _codify, and __codify_ to guarantee
    name uniqueness. 
    
        >>> r = GlobalRegistry()
        >>> r
        {}
        >>> hello = 'Hello world'
        >>> r[r(hello)] is hello
        True
        
    '''
    
    PREFIX = '__OBJ__'
    
    def __call__(self, obj, NOT_FOUND=type(None)):
        key = self.PREFIX + self.id_string(obj)
        stored = self.get(key, NOT_FOUND)
        if stored is NOT_FOUND:
            stored = self[key] = obj
        return key            
       
    def id_string(self, obj):
        return '%.8X' % id(obj)
