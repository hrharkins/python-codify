
import dataclasses
field = dataclasses.field

##############################################################################                
##############################################################################                

def cached(fn=None, name=None):
    '''
    Calls a function or method and caches the results.
        
        >>> @dataclasses.dataclass
        ... class Adder(object):
        ...     terms: tuple
        ...
        ...     @cached
        ...     def sum(self):
        ...         print("HERE")
        ...         return sum(self.terms)
        
        >>> a = Adder((3, 4, 5))
        >>> a.sum
        HERE
        12
        >>> a.sum
        12
        
        >>> del a.__cache__.sum
        >>> a.sum
        HERE
        12
        >>> a.sum
        12
        
        >>> @dataclasses.dataclass
        ... class BaseAdder(object):
        ...     terms: tuple
        ...
        ...     @cached('computed')
        ...     def compute(self):
        ...         print("ADDING")
        ...         return sum(self.terms)
        
        >>> a = BaseAdder((3, 4, 5))
        >>> a.computed
        ADDING
        12
        >>> a.computed
        12
        >>> a.compute()
        ADDING
        12
        
        >>> @dataclasses.dataclass
        ... class MultAdder(BaseAdder):
        ...     factor: float
        ...
        ...     def compute(self):
        ...         print("MULT")
        ...         return self.factor * super().compute()
        
        >>> ma = MultAdder((3, 4, 5), 2)
        >>> ma.computed
        MULT
        ADDING
        24
        >>> ma.computed
        24
        >>> ma.compute()
        MULT
        ADDING
        24
        
    '''
    
    if fn is None:
        return lambda f: cached(f, name)        # (COV-NO-DT)
    elif isinstance(fn, str):
        return lambda f: cached(f, fn)
    else:
        if name is None:
            name = fn.__name__

        getfn = (fn if fn.__name__ == name
                    else lambda t, f=fn, n=fn.__name__: getattr(t, n)())
        
        def getter(self, target, cls=None, getfn=getfn, name=name):
            if target is None:
                return self         # (COV-NO-DT)
            else:
                obj = getfn(target)
                target.__dict__[name] = obj
                return obj
        
        def setname(self, target, name, fn=fn, member=name):
            target.__cache__ = CACHE_MANAGER_PROPERTY
            # Restore the method as a method.
            setattr(target, name, fn)
            # Move the cacher to the member name.
            setattr(target, member, self)
        
        return type(name, (object,), {
            '__doc__': fn.__doc__,
            '__get__': getter,
            '__set_name__': setname,
        })()
        
@property
@dataclasses.dataclass
class CACHE_MANAGER_PROPERTY(object):
    obj: object
    
    def __delattr__(self, name):
        self.obj.__dict__.pop(name, None)

##############################################################################
##############################################################################

def delegate(src, *methods, **translations):
    '''
    
        >>> @delegate('summer', 'total')
        ... @dataclasses.dataclass
        ... class Multiplier(object):
        ...     factor: float
        ...     summer: object          # .total comes from here.
        ...
        ...     @cached
        ...     def product(self):
        ...         return self.factor * self.total
        
        >>> @dataclasses.dataclass
        ... class Adder(object):
        ...     terms: tuple
        ...
        ...     @cached('total')
        ...     def sum(self):
        ...         return sum(self.terms)
        
        >>> Multiplier(2, Adder((3, 4, 5))).product
        24
    
    '''
    
    srcfn = lambda s, n=src: getattr(s, n)
    def delegator(cls, srcfn=srcfn):
        for member, getter in dict(
            ((method, method) for method in methods),
            **translations
        ).items():
            getfn = lambda s, f=srcfn, n=getter: getattr(f(s), n)
            setattr(cls, member, cached(getfn))
        return cls            
    return delegator        

##############################################################################
##############################################################################

def alias(src):
    '''
        >>> @dataclasses.dataclass
        ... class Multisource(object):
        ...     x = alias('y')
        ...     y: int
        
        >>> obj = Multisource(y='hello')
        >>> obj.x
        'hello'
    '''
    
    class Aliaser(object):
        def __set_name__(self, cls, name):
            getter = lambda s, src=src: getattr(s, src)
            setattr(cls, name, cached(getter))
    return Aliaser()            
