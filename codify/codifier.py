
from .util import delegate, cached, alias
from .encode import codify

@delegate(
    'block',
    '__getitem__',
    '__setitem__',
    '__delitem__',
    'append',
    'extend',
)
class Codifier(object):
    def __init__(self, *block):
        self.block = list(block)
        
    def __iter__(self):
        yield from codify(..., self)
    
    def __codify__(self, registry):
        return self.block
        
class BlockCodifier(Codifier):
    def __codify__(self, registry):
        return (
            self.blockhead(registry),
            super().__codify__(registry)
        )            
        
    def blockhead(self, registry):
        return None                 # (COV-NO-DT)

class FunctionCodifier(BlockCodifier):
    '''
        >>> from codify import codify
        >>> fc = FunctionCodifier('hello')
        >>> fc.append('print("hello")')
        >>> print('\\n'.join(fc))
        def hello():
            print("hello")
        >>> fc()
        hello
    '''
    
    def __init__(self, name, args=(), block=(), kwargs=(), **kw):
        self.name = name
        self.args = list(args)
        self.kwargs = dict(kwargs, **kw)
        super().__init__(*block)
        
    @cached('fn')        
    def make_fn(self):
        return codify(self.name, self)
    __call__ = alias('fn')        

    def blockhead(self, registry):
        args = list(self.args)
        args.extend('%s=%s' % (name, registry[value]) 
                    for name, value in self.kwargs.items())
        return 'def %s(%s):' % (self.name, ', '.join(args))
