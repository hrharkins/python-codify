#!/usr/bin/env python3.8
#
# Assumes pip install -r setup/test-requirements.txt has been run (or better
# yet is in docker).
#

import sys, os, unittest, coverage, doctest, argparse, pathlib, importlib

# Make sure our code is top of the import path.
BASEDIR=os.path.dirname(os.path.dirname(os.path.realpath(sys.argv[0])))
if BASEDIR not in sys.path:
    sys.path.insert(0, BASEDIR)
    
class CoverageContext(coverage.Coverage):
    def __enter__(self):
        self.erase()
        self.start()
        return self
        
    def __exit__(self, *_args, **_kw):
        self.stop()
    
def run_doctests():
    runner = unittest.TextTestRunner()
    suite = unittest.TestSuite()
    # TODO: Find a better means of auto-generating these.
    importlib.invalidate_caches()
    for module in (mod for mod in list(sys.modules) if 'codify' in mod):
        importlib.reload(__import__(module))
        
    for module in (
            '', 
            '.util', 
            '.encode', 
            '.codifier', 
            '.registry',
        ):
        suite.addTests(doctest.DocTestSuite('codify' + module))
    return runner.run(suite)

def run_unittests():
    importlib.invalidate_caches()
    for module in (mod for mod in list(sys.modules) if 'codify' in mod):
        importlib.reload(__import__(module))
        
    runner = unittest.TextTestRunner()
    tests = unittest.TestLoader().discover(os.path.join(BASEDIR, 'tests'))
    return runner.run(tests)
    
def main(args):
    parser = build_arg_parser()
    parsed = parser.parse_args(args)
    
    testdirs = args
    sources = ['codify']
    
    # Forking into subprocesses to make sure they indepenently load codify
    
    if child_pid := os.fork():
        os.waitpid(child_pid, 0)
    else:        
        print()
        print('#############################################################')
        print('# RUNNING UNITTESTS')
        print('#############################################################')
        print()
        sys.stdout.flush()
        sys.stderr.flush()
        with CoverageContext(source=sources) as unittest_coverage:
            unittest_coverage.exclude('(NO-COV)')
            unittest_coverage.exclude('(COV-NO-UT)')
            run_unittests()
        unittest_coverage.report()
        if report_dir := parsed.report:
            report_path = pathlib.Path(report_dir)
            unittest_coverage.html_report(
                directory=str(report_path / 'coverage/unittest/html/')
            )
    
    if child_pid := os.fork():
        os.waitpid(child_pid, 0)
    else:        
        print()
        print('#############################################################')
        print('# RUNNING DOCTESTS')
        print('#############################################################')
        print()
        sys.stdout.flush()
        sys.stderr.flush()
        with CoverageContext(source=sources) as doctest_coverage:
            doctest_coverage.exclude('(NO-COV)')
            doctest_coverage.exclude('(COV-NO-DT)')
            run_doctests()
        doctest_coverage.report()
        if report_dir := parsed.report:
            report_path = pathlib.Path(report_dir)
            doctest_coverage.html_report(
                directory=str(report_path / 'coverage/doctest/html/')
            )

def build_arg_parser(parser=None):
    if parser is None:
        parser = argparse.ArgumentParser()
        
    parser.add_argument('--report', dest='report')        

    return parser

if __name__ == '__main__':
    main(sys.argv[1:])
