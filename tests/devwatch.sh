#!/bin/bash

export TERM=${TERM:-ansi}
find . -name '*.py' | entr -c python3 ./tests/runtests.py "$@"